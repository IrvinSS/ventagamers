<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Inserta datos en las tablas.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ratones')->insert([
            'nombre' => 'Mouse Logitech',
            'precio' => '500',
            'imagen' => 'https://www.cyberpuerta.mx/img/product/M/CP-LOGITECH-910-004843-1.jpg',
            ]);
        DB::table('ratones')->insert([
            'nombre' => 'Mouse Razer 8200 DPI',
            'precio' => '1200',
            'imagen' => 'https://images-na.ssl-images-amazon.com/images/I/41X8qdl8WxL._SL500_AC_SS350_.jpg',
        ]);
        DB::table('teclados')->insert([
            'nombre' => 'Teclado Lenovo Legion K200',
            'precio' => '2800',
            'imagen' => 'https://elektra.vteximg.com.br/arquivos/ids/360448-1000-1000/830578.jpg?v=636445363200200000',
        ]);
        DB::table('teclados')->insert([
            'nombre' => 'Teclado Razer Cynosa',
            'precio' => '1600',
            'imagen' => 'https://www.hd-tecnologia.com/imagenes/articulos/2017/10/Razer-Cynosa-Chroma-Pro-y-Cynosa-Chroma-nueva-linea-de-teclados-Gamers-econ%C3%B3micos-de-la-empresa.jpg',
        ]);
        DB::table('sillas')->insert([
            'nombre' => 'Silla AORUS AGC300',
            'precio' => '6500',
            'imagen' => 'https://www.cyberpuerta.mx/img/product/M/CP-AORUS-AGC300-1.jpg',
        ]);
    }
}
