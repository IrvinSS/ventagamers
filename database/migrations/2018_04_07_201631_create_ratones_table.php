<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatonesTable extends Migration
{
    /**
     * Crea la tabla de los productos de ratones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('precio');
            $table->string('imagen');
            $table->timestamps();
        });
    }

    /**
     * Elimina la tabla.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ratones');
    }
}
