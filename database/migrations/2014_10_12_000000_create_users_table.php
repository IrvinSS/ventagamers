<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Crea la tabla de usuarios.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('admin')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Elimina la tabla de usuarios.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
