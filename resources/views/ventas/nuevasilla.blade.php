@extends('layouts.app')

@section('content')
    <style>
        label {
            margin: 2px;
            padding: 2px;
        }
        input {
            padding: 1px;
            margin: 5px;
        }
    </style>
    @if (Auth::user()->admin == 1)
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Agrega una nueva silla</div>

                        <div class="panel-body" align="center">
                            <form action="{{url('crear/sillas')}}" method="post">
                                {{ csrf_field() }}
                                <div>
                                    <label>Nombre</label><br><input type="text" name="nombre" required>
                                </div>
                                <div>
                                    <label>Precio en MX</label><br><input type="text" name="precio" required>
                                </div>
                                <div>
                                    <label>URL de la imagen</label><br><input type="text" name="imagen" required>
                                </div>
                                <div>
                                    <input type="submit" name="submit" class="btn btn-primary" value="Guardar nuevo producto">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <meta  http-equiv="refresh" content="0;URL=/">
    @endif

@endsection
