@extends('layouts.app')

@section('content')
    <style>
        table, tr, th {
            margin: 10px;
            padding: 10px;
            align-self: center;
        }
    </style>
    @if (Auth::user()->admin == 1)
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Escoje que producto quieres agregar</div>
                        <table align="center">
                            <tr>
                                <th>
                                    <form action="{{ url('/crear/ratones') }}" method="get">
                                        <input type="submit" class="btn btn-primary" name="opcion" value="Ratones"/>
                                    </form>
                                </th>
                                <th>
                                    <form action="{{ url('/crear/teclados') }}" method="get">
                                        <input type="submit" class="btn btn-primary" name="opcion" value="Teclados"/>
                                    </form>
                                </th>
                                <th>
                                    <form action="{{ url('/crear/sillas') }}" method="get">
                                        <input type="submit" class="btn btn-primary" name="opcion" value="Sillas"/>
                                    </form>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <meta  http-equiv="refresh" content="0;URL=/">
    @endif
@endsection
