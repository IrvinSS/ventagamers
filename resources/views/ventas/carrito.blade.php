@extends('layouts.app')

@section('content')
    <style>
        div {
            padding: 5px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Carrito de compras</div>

                    <div class="panel-body" align="center">
                            <div class="col-md-4"> Nombre</div>
                            <div class="col-md-4"> Precio</div>
                            <div class="col-md-4"> Terminar compra</div>
                        @foreach($carritos as $carrito)
                            <div class="col-md-4">{{ $carrito->nombre }}</div>
                            <div class="col-md-4">{{ $carrito->precio }}</div>
                            <div class="col-md-4">
                                <button
                                        type="button"
                                        class="btn btn-primary btn-lg"
                                        data-toggle="modal"
                                        data-target="#favoritesModal">
                                    Comprar
                                </button>
                                {{--<form action="#" method="post">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<input class="btn btn-primary " type="submit" value="Comprar" align="center">--}}
                                    {{--<input type="hidden" name="idUsuario" value="{{ Auth::user()->id }}">--}}
                                    {{--<input type="hidden" name="idProducto" value="{{ $carrito->idProducto }}">--}}
                                {{--</form>--}}
                            </div>
                        @endforeach
                        <div class="modal fade" id="favoritesModal"
                             tabindex="-1" role="dialog"
                             aria-labelledby="favoritesModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close"
                                                data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title"
                                            id="favoritesModalLabel">Agrega metodo de pago</h4>
                                    </div>
                                    <form method="get" action="{{ url('/') }}">
                                    <div class="modal-body">
                                        <label for="correo" id="txt_correo" hidden>Correo de Paypal:</label>
                                        <input id="correo" type="text" name="correo" hidden required/>

                                        <label for="efectivo">Efectivo</label>
                                        <input id="efectivo" type="radio" value="1" name="fooby[1][]">

                                        <label for="paypal">Paypal</label>
                                        <input id="paypal" type="radio" value="1" name="fooby[1][]">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Cancelar</button>
                                        <span class="pull-right">
                                        <button type="submit" class="btn btn-primary">
                                            Agragar metodo de pago
                                        </button>
                                        </span>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" align="right">
                            <form action="{{ url('carritoLimpiar') }}" method="get">
                                <input class="btn btn-warning" type="submit" value="Quitar todo del carrito">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            /**
             * for showing edit item popup
             */

            $(document).on('click', "#edit-item", function() {
                $(this).addClass('edit-item-trigger-clicked'); //useful for identifying which trigger was clicked and consequently grab data from the correct row and not the wrong one.

                var options = {
                    'backdrop': 'static'
                };
                $('#edit-modal').modal(options)
            })

            // on modal show
            $('#edit-modal').on('show.bs.modal', function() {
                var el = $(".edit-item-trigger-clicked"); // See how its usefull right here?
                var row = el.closest(".data-row");

                // get the data
                var id = el.data('item-id');
                var name = row.children(".name").text();
                var description = row.children(".description").text();

                // fill the data in the input fields
                $("#modal-input-id").val(id);
                $("#modal-input-name").val(name);
                $("#modal-input-description").val(description);

            })

            // on modal hide
            $('#edit-modal').on('hide.bs.modal', function() {
                $('.edit-item-trigger-clicked').removeClass('edit-item-trigger-clicked')
                $("#edit-form").trigger("reset");
            })
        })

        // the selector will match all input controls of type :checkbox
        // and attach a click event handler
        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        $(document).ready(function(){
            $('#paypal').on('change',function(){
                if (this.checked) {
                    $("#correo").show();
                    $("#txt_correo").show();
                }
            })
        });

        $(document).ready(function(){
            $('#efectivo').on('change',function(){
                if (this.checked) {
                    $("#correo").hide();
                    $("#txt_correo").hide();
                }
            })
        });
    </script>
@endsection
