@extends('layouts.app')

@section('content')
    <style>
        table, th, td {
            border: 1px solid black;
            margin: 7px;
            padding: 5px;
            text-align: center;
        }
        img {
            height: 100px;
            width: 100px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Ratones</div>

                    <div class="panel-body" align="center">
                        Los mejores ratones, para mejor sensacion
                        <table>
                            <thead>
                            <tr>
                                <th> Nombre</th>
                                <th> Precio</th>
                                <th> Imagen</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($productos as $producto){ ?>
                            <tr>
                                <td><?php echo $producto->nombre ?></td>
                                <td><?php echo $producto->precio ?></td>
                                <td> <img src="<?php echo $producto->imagen ?>"></td>
                                <td>
                                    <form action="{{url('/carrito')}}" method="post">
                                        {{ csrf_field() }}
                                        <input class="btn btn-primary " type="submit" value="Agregar a carrito" align="center">
                                        <input type="hidden" name="idUsuario" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="idProducto" value="<?php echo $producto->id ?>">
                                        <input type="hidden" name="nombre" value="<?php echo $producto->nombre ?>">
                                        <input type="hidden" name="precio" value="<?php echo $producto->precio ?>">
                                        <input type="hidden" name="imagen" value="<?php echo $producto->imagen ?>">
                                    </form>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
