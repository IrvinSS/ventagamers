@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">Bienvenido!</div>

                    <div class="panel-body" style="text-align: center">
                        @if (Auth::guest())
                            <form action="{{ url('/register') }}" method="get">
                                <label class="">Crea una cuenta para poder comprar productos!</label><br>
                                <input type="submit" class="btn btn-primary" value="Crear cuenta" name="crear" id="btn-register" />
                            </form>
                            <form action="{{ url('/login') }}" method="get">
                                <label class="">¿Ya tienes cuenta?</label><br>
                                <input type="submit" class="btn btn-primary" value="Inicia sesion!" name="inicio" id="btn-login" />
                            </form>
                        @else
                            La mejor tienda virtual para conseguir tus productos gamers
                        @endif
                            @if (session('notification'))
                                {{ session('notification') }}
                            @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
