<!DOCTYPE html>
<html lang="es">
<head>
    <title>Confirmar</title>
    <meta charset="utf-8" />
</head>
<body class="homepage">
    <a href="{{ url('register/verify', $confirmation_code) }}">
        Confirma tu correo en la pagina.
    </a>
</body>
</html>