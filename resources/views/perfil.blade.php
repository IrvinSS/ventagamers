@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hola, {{ Auth::user()->name }}</div>

                <div class="panel-body">
                    <label>Esta es tu pagina de perfil!</label><br><br>

                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Cerrar sesion</a></li>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
