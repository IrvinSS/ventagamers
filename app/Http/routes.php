<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/perfil', 'HomeController@perfil');



Route::get('/ratones', 'VentasController@ventaratones');

Route::get('/teclados', 'VentasController@ventateclados');

Route::get('/sillas', 'VentasController@ventasillas');

//Shopping Card
Route::get('/carrito', 'HomeController@carrito');
Route::post('/carrito', 'HomeController@agregarCarrito');
Route::get('carritoLimpiar', 'HomeController@limpiarCarrito');


Route::get('/nuevo', 'HomeController@nuevo');

Route::get('/crear/ratones', 'HomeController@nuevosratones');

Route::post('/crear/ratones', 'HomeController@crearnuevoraton');

Route::get('/crear/teclados', 'HomeController@nuevoteclado');

Route::post('/crear/teclados', 'HomeController@crearnuevoteclado');

Route::get('/crear/sillas', 'HomeController@nuevasilla');

Route::post('/crear/sillas', 'HomeController@crearnuevasilla');

//E-mail verification
Route::get('/register/verify/{code}', 'Auth\AuthController@verify');