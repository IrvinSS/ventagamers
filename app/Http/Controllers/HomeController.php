<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Auth;
use Mockery\Exception;
use App\Carrito;

class HomeController extends Controller
{
    /**
     * Verifica si el usuario esta registrado.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Te muestra el html del perfil
     */
    public function perfil()
    {
        return view('perfil');
    }

    /**
     * Te muestra el html para agregar un producto
     */
    public function nuevo(){
        return view('/ventas/nuevo');
    }

    /**
     * Te muestra el html para agregar un raton
     */
    public function nuevosratones(){
        return view('ventas/nuevoratones');
    }

    /**
     * Funcion para agregar el producto a la base de datos
     */
    public function crearnuevoraton(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen);

        DB::table('ratones')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    /**
     * Te muestra el html para agregar un nuevo teclado
     */
    public function nuevoteclado(){
        return view('ventas/nuevosteclados');
    }

    /**
     * Funcion para agregar el teclado a la base de datos
     */
    public function crearnuevoteclado(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen);

        DB::table('teclados')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    /**
     * Te muestra el html para agregar una nueva silla
     */
    public function nuevasilla(){
        return view('ventas/nuevasilla');
    }

    /**
     * Funcion para agregar la silla a la base de datos
     */
    public function crearnuevasilla(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen);

        DB::table('sillas')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    /**
     * Te muestra el html del carrito con los productos si es que existen
     */
    public function carrito(){
        $userId = Auth::user()->id;
        $carrito = DB::table('carritos')->where('idUsuario','=',$userId)->get();

        return view('ventas/carrito', ['carritos' => $carrito]);
    }

    /**
     * Funcion para agregar un producto al carrito y te regresa el html del carrito
     */
    public function agregarCarrito(Request $request){
        $idUsuario = $request->input('idUsuario');
        $idProducto = $request->input('idProducto');
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');
        try{
            $data = array('nombre'=>$nombre,'idUsuario'=>$idUsuario,"idProducto"=>$idProducto,"precio"=>$precio,
                "imagen"=>$imagen);

            DB::table('carritos')->insert($data);
        }catch (Exception $exception){
            return view('errors/404');
        }
        return $this->carrito();
    }

    /**
     * Quita todos los productos de usuario del carrito
     */
    public function limpiarCarrito(){
        $userId = Auth::user()->id;
        DB::table('carritos')->where('idUsuario', $userId)->delete();

        return redirect('/');
    }
}
