<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teclados;
use App\Ratones;
use App\Sillas;

use App\Http\Requests;

class VentasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Te regresa el html de los produdctos en venta de ratones
     */
    public function ventaratones(){
        $productos = Ratones::all();
        return view('ventas/ratones')->with('productos', $productos);
    }

    /**
     * Te regresa el html de los produdctos en venta de teclados
     */
    public function ventateclados(){
        $productos = Teclados::all();
        return view('ventas/teclados')->with('productos', $productos);
    }

    /**
     * Te regresa el html de los produdctos en venta de sillas
     */
    public function ventasillas(){
        $productos = Sillas::all();
        return view('ventas/sillas')->with('productos', $productos);
    }
}
