<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sillas extends Model
{
    protected $table = 'sillas';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen',
    ];
}
