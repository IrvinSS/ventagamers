<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teclados extends Model
{
    protected $table = 'teclados';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen',
    ];
}
