<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratones extends Model
{
    protected $table = 'ratones';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen',
    ];
}
